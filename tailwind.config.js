module.exports = {

    purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],   //Configure Tailwind to remove unused styles in production
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {},
    },
    variants: {
        extend: {},
    },
    plugins: [],
    
}
